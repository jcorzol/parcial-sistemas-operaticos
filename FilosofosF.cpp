#include <iostream>
#include <mutex>
#include <thread>
#include <ctime>
#include <chrono>
using namespace std;
const int numfilosofos=5; //cantidad de filosofos
thread filosofos[numfilosofos];// se creaa los threads
mutex mtx[numfilosofos]; //mutex para controlar los hilos
int estomago[numfilosofos];//array de estomagos igual al numero de filosofos

int plato[numfilosofos];//se crea un plato para cada filosofo

void inivalores(){ //funcion para dar un estomago y plato a cada filosofo
    for(int i=0; i<numfilosofos; i++){
        estomago[i]=100;//se inicializa en 100 la energia de cada filosofo
        plato[i]=20;//tendran 20 fideos por cada filosofo
    }
}

void print(string str){
    mutex cout_mutex;
    cout_mutex.lock();
    cout<<str<<endl;
    cout_mutex.unlock();
}

void pensar(int id){
    time_t timeInicial, timeFinal;
    timeInicial = time(0);
    int num=1000+rand()%(2000-100);
    this_thread::sleep_for(chrono::milliseconds(num)); //el pensar tiene un tiempo random
    timeFinal = time(0);
    int a=timeFinal-timeInicial;
    print("El filosofo " + to_string(id) + " esta pensando");
    estomago[id]=estomago[id]-25;//si piensa va dismuyendo la energia del filosofo
    if(a >= 2 and estomago[id]<=100){
      print("El filosofo " + to_string(id) + " murio de hambre"); // el filosofo muere de hambre 
    }
    
}

//solucion de seccion critica usando mutex (semaforos)
bool comer(int id, int izquierda, int derecha){ 
    while(1) if (mtx[izquierda].try_lock()) {
        if(estomago[id]>=125){//controlamos que el filosofo deje de comer cuando tenga 125 de enegia
            return true; //retorna true y no realiza el proceso de los palillos
        }

        print("El filosofo " + to_string(id) + " tiene el palillo " + to_string(izquierda));

        if (mtx[derecha].try_lock()) {
            int num=1000+rand()%(2000-100);//se genera un tiempo ramdon cada vez que come 
            this_thread::sleep_for(chrono::milliseconds(num));
            print("El filosofo " + to_string(id) + " tiene el palillo " + to_string(derecha) + 
                "\n El filosofo " + to_string(id) + " come");
            estomago[id]=estomago[id]+25;//aumento de energia cada vez que come 
            return true;
        } 
        else {
            mtx[izquierda].unlock(); //si no puede agarrar el segundo palillo suelta el que tenia
            pensar(id);
        }
    }
    return false;
}



void inicio(int filosofo){
    int lIndex = min(filosofo, (filosofo + 1) % (5));
    int rIndex = max(filosofo, (filosofo + 1) % (5));

    while (plato[filosofo]-- > 0){
        if (comer(filosofo, lIndex, rIndex)) { 
            mtx[lIndex].unlock(); // se baja el tenedor derecho
            mtx[rIndex].unlock(); // se baja el tenedor izquierdo
            this_thread::sleep_for(std::chrono::milliseconds(600));
        }    
    }

}

int main(){ 
    inivalores();// funcion para inicializar valores
    for (int i = 0; i < 5; ++i) filosofos[i] = std::thread(inicio, i);
    for (int i = 0; i <5; ++i) filosofos[i].join();
    for (int i = 0; i < 5; ++i){
        cout << i <<" = "<<estomago[i]<<endl;
    }   
}